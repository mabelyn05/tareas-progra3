
package com.mycompany.algoritmo;

import java.util.Scanner;

/**
 *
 * @author mjber
 */
public class algoritmo {

    
    public  int distancia[] = new int[10]; // sirve para calcular distancia 
    public  int costo[][] = new int[10][10];
    
    // metodo para calcular la ruta mas corta 
    public void calcular(int N, int origen){
       int flag[] = new int[N+1];
       int i,k,c,minimo;// estas al declararse valen 0 por default
       int minpos=1;
        //LLEVAMOS AL VECTOR DISTANCIA  LA PRIMERA FILA DE LA MATRIZ
           for (i = 1; i <= N; i++) {
               flag[1]=0;
               this.distancia[i]=this.costo[origen][i];// costo es igual a 0
           }
          c=2;
          while(c<=2){
              minimo=99;
              for ( k = 1; k <=N; k++) {
                  if (this.distancia[k]<minimo && flag[k] !=1) 
                  {
                      minimo=this.distancia[i];
                      minpos=k;
                  }
                  flag[minpos]=1;
                  c++;
                  for ( k = 1; k <=N; k++) {
                      if (this.distancia[minpos] + this.costo[minpos] [k] < this.distancia[k] && flag[k] !=1) {
                           this.distancia[k]=this.distancia[minpos] + this.costo[minpos][k];

                      }

                  }

              }
       }
 }
    
    public static void main(String[] args) {
       int c=1;
       int nodos,origen;
       int i,j;
       String salida ="nodo\tA \tb \tc \td\n";
       
       Scanner in = new Scanner(System.in);
        System.out.println("ingrese el numero de nodos");
        nodos = in.nextInt();
        DIAGRAMA d = new DIAGRAMA();
        System.out.println("ingrese el costo de los pesos de la matriz separados por espacios");
        for ( i = 1; i <= nodos; i++) {
            salida +=c++ +"\t";
            for ( j = 1; j <= nodos; j++) {
                d.costo[i][j]= in.nextInt();
                salida += d.costo[i][j] + "\t";
            }
            salida +="\n";
        }
       
        
        System.out.println("Ingrese el vertice de origen");
        origen=in.nextInt();
        d.calcular(nodos, origen);
        System.out.println("La ruta mas corta desde el nodo \t"+ origen +"\t a todos los demas es \n");
        for ( i = 1; i <= nodos; i++) {
            if (i != origen) {
                 System.out.println("origen: "+origen+"\t destino:"+i+"\t costo minnimo es :"+ d.distancia[i]+"\t");
            }
        }
        System.out.println("mostrando la matriz de costo");
        for ( i = 1; i <=nodos; i++) {
            for ( j = 0; j <=nodos; j++) {
                System.out.println(d.costo[i][j] +"\t");
            }
            System.out.println("");
        }
    
    
    }
    
}
