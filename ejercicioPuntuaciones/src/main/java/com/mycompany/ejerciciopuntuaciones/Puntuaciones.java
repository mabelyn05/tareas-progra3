
package com.mycompany.ejerciciopuntuaciones;


public class Puntuaciones {

    public static void main(String[] args) {
        
        int[] arreglo = { 1,2,3,4,5};
        puntuacion(arreglo);
                
    }
    
    public static void puntuacion(int [] arreglo){
        
        int puntos = 0;

        for(int i=0; i<arreglo.length; i++)
        {
            if ((arreglo[i] % 2) != 0)
            {
                if (arreglo[i] == 5)
                {
                    puntos += 5;
                }
                else
                {
                    puntos += 3;
                }
            }
            else
            {
                puntos++;
            }

        }

            System.out.println(puntos);
    }
    
}
